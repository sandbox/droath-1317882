<?php

/**
 * @file
 * Implements the admin configurations for taxonomy field UI.
 */

/**
 * Form API; menu callback for taxonomy field UI name form.
 */
function taxonomy_field_ui_name_edit_form($form, $form_state, $vocabulary_name, $field_name) {
  $loaded_data = _taxonomy_field_ui_load($vocabulary_name);
  $settings = $loaded_data[$field_name];

  $form['#field_name'] = $field_name;
  $form['#vocabulary_name'] = $vocabulary_name;
  _taxonomy_field_ui_common_form_elments($form, $settings);
  return $form;
}

/**
 * Form API; menu callback for taxonomy field UI description form.
 */
function taxonomy_field_ui_description_edit_form($form, $form_state, $vocabulary_name, $field_name) {
  $loaded_data = _taxonomy_field_ui_load($vocabulary_name);
  $settings = $loaded_data[$field_name];

  $form['taxonomy_field_ui_description_hidden'] = array(
    '#type' => 'checkbox',
    '#title' => t("Hidden"),
    '#description' => t('If selected the field will be hidden on the editing form.'),
    '#default_value' => isset($settings['taxonomy_field_ui_description_hidden']) ? $settings['taxonomy_field_ui_description_hidden'] : FALSE,
  );
  $form['taxonomy_field_ui_text_processing'] = array(
    '#type' => 'radios',
    '#title' => t('Text processing'),
    '#options' => array(
      'plain_text' => t('Plain text'),
      'filtered_html' => t('Filtered text (user selects text format)'),
    ),
    '#default_value' => isset($settings['taxonomy_field_ui_text_processing']) ? $settings['taxonomy_field_ui_text_processing'] : 'filtered_html',
  );
  $form['taxonomy_field_ui_rows'] = array(
    '#type' => 'textfield',
    '#title' => t('Rows'),
    '#required' => TRUE,
    '#default_value' => isset($settings['taxonomy_field_ui_rows']) ? $settings['taxonomy_field_ui_rows'] : 5,
    '#element_validate' => array('_taxonomy_field_ui_rows_validate'),
  );
  $form['#field_name'] = $field_name;
  $form['#vocabulary_name'] = $vocabulary_name;
  _taxonomy_field_ui_common_form_elments($form, $settings);
  return $form;
}

/**
 * Form API; submit handler for the taxonomy field UI editing forms.
 */
function taxonomy_field_ui_editing_form_submit($form, &$form_state) {
  $options = array();
  $options['vocabulary_name'] = $form['#vocabulary_name'];

  foreach ($form_state['values'] as $key => $data) {
    if (preg_match('/taxonomy_field_ui/', $key) && $data != '') {
      $options['save_data'][$form['#field_name']][$key] = $data;
    }
  }
  _taxonomy_field_ui_save($options);
  $form_state['redirect'] = 'admin/structure/taxonomy/' . $options['vocabulary_name'] . '/fields';
}

/**
 * Form API; delete confirm handler for the taxonomy field UI editing forms.
 */
function taxonomy_field_ui_delete_confirm($form, $form_state, $vocabulary_name, $field_name) {
  $options = array();
  $options['field_name'] = $field_name;
  $options['vocabulary_name'] = $vocabulary_name;
  $form_state['taxonomy_field_ui_options'] = $options;
  return confirm_form(
    $form,
    t('Are you sure you want to delete the "%field_name" field data?', array('%field_name' => $field_name)),
    'admin/structure/taxonomy/' . $vocabulary_name . '/fields',
    t('<strong>NOTE:</strong> This will not delete the field itself. Only the configuration options will be deleted. <br/> This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Form API; delete submit handler for the taxonomy field UI editing forms.
 */
function taxonomy_field_ui_delete_confirm_submit($form, $form_state) {
  $options = $form_state['taxonomy_field_ui_options'];
  _taxonomy_field_ui_delete($options);
  drupal_set_message(t('Configurations options have been deleted for %field_name.', array('%field_name' => $options['field_name'])));
  drupal_goto('admin/structure/taxonomy/' . $options['vocabulary_name'] . '/fields');
}

/**
 * Helper function; common form elements.
 */
function _taxonomy_field_ui_common_form_elments(&$form, $settings) {
  $default_label = $form['#field_name'] == 'name' ? t('Name') : t('Description');

  $form['taxonomy_field_ui_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#required' => TRUE,
    '#default_value' => isset($settings['taxonomy_field_ui_label']) ? $settings['taxonomy_field_ui_label'] : $default_label,
  );
  $form['taxonomy_field_ui_help_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Help text'),
    '#description' => t('Instructions to present to the user below this field on the editing form.'),
    '#default_value' => isset($settings['taxonomy_field_ui_help_text']) ? $settings['taxonomy_field_ui_help_text'] : NULL,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );
  $form['#submit'][] = 'taxonomy_field_ui_editing_form_submit';
}

/**
 * Helper function; validates the rows element form.
 */
function _taxonomy_field_ui_rows_validate($element, $form_state, $form) {
  if (!is_numeric($element['#value'])) {
    form_error($element, t('%label field is not an interger.', array('%label' => $element['#title'])));
  }
}

